<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

require_once($CFG->dirroot.'/grade/export/lib.php');
require_once($CFG->dirroot.'/local/mentorweb/classes/mentorweb.php');

class grade_export_mentorweb extends grade_export {

    public $plugin = 'mentorweb';

    public $separator; // default separator

    /**
     * Constructor should set up all the private variables ready to be pulled
     * @param object $course
     * @param int $groupid id of selected group, 0 means all
     * @param stdClass $formdata The validated data from the grade export form.
     */
    public function __construct($course, $groupid, $formdata) {
        parent::__construct($course, $groupid, $formdata);
        $this->separator = $formdata->separator;

        // Overrides.
        $this->usercustomfields = true;
    }

    public function get_export_params() {
        $params = parent::get_export_params();
        $params['separator'] = $this->separator;
        return $params;
    }

    public function print_grades() {
        global $CFG;

        $export_tracking = $this->track_exports();

        $strgrades = get_string('grades');

        // Print all the lines of data.
        $geub = new grade_export_update_buffer();
        $gui = new graded_users_iterator($this->course, $this->columns, $this->groupid);
        $gui->require_active_enrolment($this->onlyactive);
        $gui->allow_user_custom_fields($this->usercustomfields);
        $gui->init();

        $by_cmid = array();
        while ($userdata = $gui->next_user()) {

            $exportdata = array();
            $user = $userdata->user;

            foreach ($userdata->grades as $itemid => $grade) {
                if ($grade->grade_item->itemtype != 'mod') {
                    continue;
                }
                $gi = $grade->grade_item;
                if (!$cm = get_coursemodule_from_instance($gi->itemmodule, $gi->iteminstance, $gi->courseid)) {
                    continue;
                }
                if ($export_tracking) {
                    $status = $geub->track($grade);
                }

                foreach ($this->displaytype as $gradedisplayconst) {
                    $by_itemid[$cm->id][$user->id] = $this->format_grade($grade, $gradedisplayconst);
                    if ($by_itemid[$cm->id][$user->id] == '-') {
                        $by_itemid[$cm->id][$user->id] = 0;
                    }
                }
            }
        }
        $to_send = array();
        foreach ($by_itemid as $cmid => $users) {
            $listAlunoNota = array();
            foreach ($users as $userid => $grade) {
                $listAlunoNota[] = array('codigoIntegracaoAluno' => $userid, 'nota' => $grade);
            }

            $to_send[] = array('codigoIntegracaoAvaliacao' => $cmid,
                               'listAlunoNota' => $listAlunoNota);
        }
        $logs = mentorweb::atualiza_notas($to_send);
        $gui->close();
        $geub->close();
        $output = html_writer::tag('h2', 'Resultados da exportação');
        $output .= html_writer::start_tag('table', array('class' => 'generaltable'));
        $output .= html_writer::start_tag('tr');
        $output .= html_writer::tag('th', 'Dados enviados');
        $output .= html_writer::tag('th', 'Resposta');
        $output .= html_writer::end_tag('tr');
        foreach ($logs as $l) {
            $output .= html_writer::start_tag('tr');
            $output .= html_writer::tag('td', $l['request']);

            if (isset($l['response']->codigoErro)) {
                $text = $l['response']->codigoErro ;
            } elseif (isset($l['response']->observacaoRetornoIntegracao)) {
                $text = $l['response']->observacaoRetornoIntegracao;
            } else {
                $text = $l['response']->descricaoRetornoIntegracao . '<br/>';
                foreach ($l['response']->listAlunoNota as $a) {
                    $text .= 'Aluno: ' . $a->codigoIntegracaoAluno .
                             ', Nota: ' . $a->nota ;
                }
            }
            $output .= html_writer::tag('td', $text);
            $output .= html_writer::end_tag('tr');
        }
        $output .= html_writer::end_tag('table');
        $output .= html_writer::link(new moodle_url('/grade/export/mentorweb/index.php', array('id' => $this->course->id)),
                                     get_string('back'));
        return $output;
    }
}
