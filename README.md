Moodle Grade Export MentorWeb
=============================

Este plugin integra o Moodle com o Mentor Web para registro de notas.

Instalação
----------

Você precisa ter primeiramente configurado o plugin [Moodle Local MentorWeb](https://gitlab.com/danielneis/moodle-local_mentorweb).

Na raiz do seu Moodle, execute:

    $ git clone https://gitlab.com/danielneis/moodle-local_mentorweb local/mentorweb
        
Se você não utiliza o git, pode baixar a versão mais atual deste plugin no endereço:

    https://gitlab.com/danielneis/moodle-local_mentorweb/repository/master/archive.zip
                
Ao descompactar este arquivo, será criada uma pasta com arquivos. Certifique-se de colocar esta pasta dentro da sua instalação do Moodle, na pasta "local", com nome "mentorweb".
                    
Visite seu Moodle como administrador para finalizar a instalação.

Utilização
----------

Este é um plugin de exportação de notas. Após a instalação, você poderá acessá-lo indo no curso que deseja exportar as notas, acessando seu livro de notas e indo na sessão "Exportar".

Lá na sessão "Exportar", dentro do livro de notas, será apresentada a opção "MentorWeb".

Ao acessar a sessão "MentorWeb", você verá a lista de itens de nota. Selecione aqueles que você quer enviar para o Mentor Web e clique em "Download".

Pronto! Você será redirecionado para a mesma página com uma mensagem de sucesso.
